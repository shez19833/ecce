<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Shez\WeatherForecast\Services\WeatherService;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function show(Request $request, WeatherService $weatherService)
    {
        $forecast = $weatherService->getWeather($request->ipaddress);

        return view('welcome', compact('forecast'));
    }
}
