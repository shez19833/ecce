
#Setup

* composer require this package
* vendor publish this package `php artisan vendor:publish`
* Register API Keys for Open Weather Map Service & IPInfoDB service
* ADD the keys to your .env settings 

```OPEN_WEATHER_MAP_API_KEY``` and
```IP_INFO_DB_API_KEY``` 

* run `php artisan web:get-weather` and when prompted enter an ip address
