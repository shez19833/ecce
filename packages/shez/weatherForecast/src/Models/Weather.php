<?php

namespace Shez\WeatherForecast\Models;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'weather';
}
