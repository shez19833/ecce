<?php

namespace Shez\WeatherForecast\Models;

use Illuminate\Database\Eloquent\Model;

class Reading extends Model
{
    protected $guarded = [];

    protected $increments = false;

    public function weather()
    {
        return $this->belongsTo(Weather::class);
    }
}
