<?php

namespace Shez\WeatherForecast\Services\IPTranslatorServices;

use GuzzleHttp\Client;


class IpInfoDBService implements IPTranslatorInterface {

    public function getLocationForIP($ipAddress) : array
    {
        $response = \Cache::remember($ipAddress, config('weatherService.cacheminutes')*60, function() use ($ipAddress)
        {
            $key = config('weatherService.ipaddress.ipinfodb.key');
            $url = 'http://api.ipinfodb.com/v3/ip-city/?key=' . $key . '&ip=' . $ipAddress;

            $client = new Client();
            $response = $client->request('GET', $url);

            // TODO try/catch etc
            return $response->getBody()->getContents();
        });

        $array = explode(";", $response);
        return [
          'city' => $array[6],
          'countryCode' => $array[3],
        ];
    }
}