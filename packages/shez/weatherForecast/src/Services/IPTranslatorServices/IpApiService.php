<?php

namespace Shez\WeatherForecast\Services\IPTranslatorServices;

use GuzzleHttp\Client;


class IpApiService implements IPTranslatorInterface {

    public function getLocationForIP($ipAddress) : array
    {
        $response = \Cache::remember($ipAddress, config('weatherService.cacheminutes')*60, function() use ($ipAddress)
        {
            $url = 'http://ip-api.com/json/?' . $ipAddress;

            $client = new Client();
            $response = $client->request('GET', $url);

            // TODO try/catch etc
            return $response->getBody()->getContents();
        });

        $response = json_decode($response);

        return [
          'city' => $response->city,
          'countryCode' => $response->countryCode,
        ];
    }
}