<?php

namespace Shez\WeatherForecast\Services\IPTranslatorServices;

interface IPTranslatorInterface {

    public function getLocationForIP($ipAddress) : array;

}