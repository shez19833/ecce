<?php

namespace Shez\WeatherForecast\Services\WeatherInformationServices;

use GuzzleHttp\Client;
use Illuminate\Contracts\Config\Repository;


class OpenWeatherMapService implements WeatherInformationInterface {

    public function getWeatherForLocation($city, $countryCode) : array
    {
        $query = $city .',' . $countryCode;

        $response = \Cache::remember($query, config('weatherService.cacheminutes')*60, function() use ($query)
        {
            $key = config('weatherService.weather.openweathermap.key');
            $url = 'http://api.openweathermap.org/data/2.5/weather?appid=' . $key . '&q=' . $query;

            try {
                $client = new Client();
                $response = $client->request('GET', $url);
            } catch (\Exception $e) {
                \Log::info('something went wrong' . $e->getMessage());
                throw $e;
            }

            return $response->getBody()->getContents();
        });

        $response = json_decode($response);

        return [
            'weather' => $response->weather[0]->description,
            'temp' => $response->main->temp,
            'icon' => 'http://openweathermap.org/img/w/' . $response->weather[0]->icon . '.png',
        ];
    }
}