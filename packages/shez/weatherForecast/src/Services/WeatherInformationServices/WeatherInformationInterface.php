<?php

namespace Shez\WeatherForecast\Services\WeatherInformationServices;

interface WeatherInformationInterface {

    public function getWeatherForLocation($city, $countryCode) : array;

}