<?php

namespace Shez\WeatherForecast\Services;

use Shez\WeatherForecast\Models\Reading;
use Shez\WeatherForecast\Models\Weather;
use Shez\WeatherForecast\Services\IPTranslatorServices\IPTranslatorInterface;
use Shez\WeatherForecast\Services\WeatherInformationServices\WeatherInformationInterface;

class WeatherService {

    protected $ipTranslator, $weatherInformation;

    public function __construct(IPTranslatorInterface $ipTranslator, WeatherInformationInterface $weatherInformation)
    {
        $this->ipTranslator = $ipTranslator;
        $this->weatherInformation = $weatherInformation;
    }

    public function getWeather($ipAddress)
    {
        // TODO ideally maybe check if within last 3 hours or something..
        // TODO also use repository pattern here....
        $record = Reading::with('weather')->whereIpAddress($ipAddress)->first();

        if ( ! $record ) {

            try {
                $location = $this->ipTranslator->getLocationForIP($ipAddress);
            } catch (\Exception $e) {
                \Log::info('Couldnt get IP translation for ' . $ipAddress);
                return null;
            }

            try {
                $info = $this->weatherInformation->getWeatherForLocation($location['city'], $location['countryCode']);
            } catch (\Exception $e) {
                \Log::info('Couldnt get Weather Info for ' . json_encode($location));
                return null;
            }

            $weather = Weather::firstOrCreate([
                'condition' => $info['weather'],
                'icon' => $info['icon'],
             ]);

            $record = Reading::create([
                'ip_address' => $ipAddress,
                'temperature' => $info['temp'],
                'weather_id' => $weather->id,
            ]);

            $record->load('weather');
        }

        return [
            'weather' => $record->weather->condition,
            'temp' => $record->temperature,
            'icon' => $record->weather->icon,
        ];
    }
}
