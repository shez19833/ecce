<?php

namespace Shez\WeatherForecast;

use Illuminate\Support\ServiceProvider;
use Shez\WeatherForecast\Console\Commands\GetWeatherForecast;
use Shez\WeatherForecast\Services\IPTranslatorServices\IpApiService;
use Shez\WeatherForecast\Services\IPTranslatorServices\IpInfoDBService;
use Shez\WeatherForecast\Services\IPTranslatorServices\IPTranslatorInterface;
use Shez\WeatherForecast\Services\WeatherInformationServices\OpenWeatherMapService;
use Shez\WeatherForecast\Services\WeatherInformationServices\WeatherInformationInterface;
use Shez\WeatherForecast\Services\WeatherService;

class WeatherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/weatherService.php',
            config_path('weatherService.php')
        );

        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        if ($this->app->runningInConsole()) {
            $this->commands([
                GetWeatherForecast::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     * @throws \Exception
     */
    public function register()
    {
        $this->app->bind(
            WeatherService::class, function($app) {
            return new WeatherService($this->getTranslatorService(), $this->getWeatherService());
        });
    }

    private function getTranslatorService()
    {
        switch(config('weatherService.ipaddress.default')) {
            case 'ipinfodb':
                return new IpInfoDBService();

            case 'ipapi':
                return new IpApiService();

            default:
                throw new \Exception('Doesnt exist');
        }
    }

    private function getWeatherService()
    {
        switch(config('weatherService.weather.default')) {
            case 'openweathermap':
                return new OpenWeatherMapService();

            default:
                throw new \Exception('Doesnt exist');
        }
    }
}
