<?php

namespace Shez\WeatherForecast\Console\Commands;

use Illuminate\Console\Command;
use Shez\WeatherForecast\Services\WeatherService;

class GetWeatherForecast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'web:get-weather';
    protected $weatherService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Provide an IP address and it will get the weather for you!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WeatherService $weatherService)
    {
        parent::__construct();

        $this->weatherService = $weatherService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ipAddress = $this->ask('Please provide an IP Address');

        if ( ! filter_var($ipAddress, FILTER_VALIDATE_IP) )
        {
            $this->warn('IP Address was wrong!');
        }

        $this->info( json_encode($this->weatherService->getWeather($ipAddress)) );
    }
}
