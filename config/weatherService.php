<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'cacheminutes' => env('API_CACHE_MINUTES', 5),

    'weather' => [

        'default' => env('WEATHER_API', 'openweathermap'),

        'openweathermap' => [
            'key' => env('OPEN_WEATHER_MAP_API_KEY'),
        ],
    ],

    'ipaddress' => [

        'default' => env('ADDRESS_API', 'ipinfodb'),

        'ipinfodb' => [
            'key' => env('IP_INFO_DB_API_KEY'),
        ],

        'ipapi' => [
            //
        ],
    ]

];
