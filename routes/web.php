<?php

use Shez\WeatherForecast\Services\IPTranslatorServices\IpInfoDBService;
use Shez\WeatherForecast\Services\WeatherInformationServices\OpenWeatherMapService;
use \Shez\WeatherForecast\Services\WeatherService;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@show')->name('home.show');

Route::get('/test', function (WeatherService $w) {

    return $w->getWeather('68.96.102.16');
});
